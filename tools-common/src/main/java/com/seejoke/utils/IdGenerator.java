package com.seejoke.utils;

import com.seejoke.date.FormatUtil;

import java.util.Calendar;


public final class IdGenerator  {
	   private static final int maxId = 99;
	    private static int curId = 0;
	    private static final Object lock = new Object();
	    public static String getId() { //32
	        return getTimeString()  + getSeqId("#000000") + getRandomString(8);
	    }

	    public static String getMethodId(String method) { //32
	        return method.toUpperCase() + getTimeString()  + getSeqId("#00000") + getRandomString(8);
	    }

	    public static String getUUId() { //32
	        return getTimeString() + getSeqId("#000000") ;
	    }

	    private static String getShortId() { //25
	        return getTimeString() + getSeqId("#000000") + getRandomString(2);
	    }

	    public static String getOrderId(){//20位订单号
	    	return getRandomString(3)+getTimeString();
	    }

	    public static int getSettId(){
	    	return Integer.parseInt(getRandomString(11));
	    }

	    public static String getSettlId(){
	    	return getRandomString(8);
	    }

	    private static String getTimeString() { //17
	        Calendar calendar = Calendar.getInstance();
	        return FormatUtil.format(calendar.getTime(),"yyyyMMddHHmmssSSS");
	    }

	    public static String getDJQOrderId(){//15登机区订单号
	    	  Calendar calendar = Calendar.getInstance();
	    	  String time=FormatUtil.format(calendar.getTime(),"yyMMddHH");
	    	  return "D"+time+getSeqId("#00")+getRandomString(5);
	    }

	    public static String getDTRefundId(){//15登机区退款单号
	    	  Calendar calendar = Calendar.getInstance();
	    	  String time=FormatUtil.format(calendar.getTime(),"yyMMddHH");
	    	  return "T"+time+getSeqId("#00")+getRandomString(5);
	    }

	    public static String getCode(){//8 code
	    	return "C"+getSeqId("#000")+getRandomString(4);
	    }

	    public static String getToken(){ //40 token
	    	return "TOKEN"+getTimeString()+getSeqId("#00000")+getValidate()+getRandomString(7);
	    }

	    public static String pay(){ //32 pay
	    	return "PAY"+getSeqId("#00000")+getValidate()+getRandomString(18);
	    }

	    private static String getSeqId(String formatType) {
	        synchronized (lock) {
	            if (curId >= maxId) {
	                curId = 0;
	            }
	            return FormatUtil.format(++curId,formatType);
	        }
	    }

	    public static String getRandomString(int count) {
	        StringBuffer sb = new StringBuffer(count);
	        for (int i = 0; i < count; i++) {
	            int psd = (int) (Math.random() * (26 * 2 + 10));
	            if (psd >= 26 + 10) { //a~z
	                char a = (char) (psd + 97 - 10 - 26);
	                sb.append(String.valueOf(a));
	            } else if (psd >= 10) { //A~Z
	                char a = (char) (psd + 65 - 10);
	                sb.append(String.valueOf(a));
	            } else { //0~9
	                sb.append(String.valueOf(psd));
	            }
	        }
	        return sb.toString().toUpperCase();
	    }

	    /**
	     * getValidate
	     * 升成6位随机码
	     * @return
	     */
	    public static String getValidate(){
			String str = "";
			str += (int)(Math.random()*9+1);
			for(int i = 0; i < 5; i++){
				str += (int)(Math.random()*10);
			}
			System.out.println(str);
			return str;
		}

	    /**
	     * 商品规格码
	     * @return
	     */
	    public static String getSpectionTimeID(){
	    	String str = "";
	    	String times = String.valueOf(System.currentTimeMillis());
			times = times.substring(times.length()-6,times.length());
			String random = String.valueOf(Math.round(Math.random() * 100 + Math.random() * 10));
			str = times + random;
	    	return str;
	    }

	    public static void main(String[] args) {
			for (int i = 0; i < 150; i++) {
				System.out.println(getMethodId("a"));
			}
	    }
}
