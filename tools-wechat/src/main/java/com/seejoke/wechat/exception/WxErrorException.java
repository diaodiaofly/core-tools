package com.seejoke.wechat.exception;

import com.seejoke.wechat.entity.result.WxError;


/**
 * 异常类
 *
 * @author diaodiaofly
 * @version 1.0
 * @date 2018/4/22 10:45
 */
public class WxErrorException extends Exception {

    private WxError error;

    public WxErrorException(WxError error) {
        super(error.toString());
        this.error = error;
    }

    public WxErrorException(String msg) {
        super(msg);
    }

    public WxError getError() {
        return error;
    }

}
