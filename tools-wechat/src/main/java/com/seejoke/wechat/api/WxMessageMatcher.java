package com.seejoke.wechat.api;

import com.seejoke.wechat.entity.WxXmlMessage;

public interface WxMessageMatcher {

	boolean match(WxXmlMessage message);

}
