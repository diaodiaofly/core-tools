package com.seejoke.wechat.api;

import com.seejoke.wechat.exception.WxErrorException;

public interface WxErrorExceptionHandler {

	void handle(WxErrorException e);

}
