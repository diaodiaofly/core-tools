package com.seejoke.wechat.api;

import com.seejoke.wechat.entity.AbstractWxXmlOutMessage;
import com.seejoke.wechat.entity.WxXmlMessage;

import java.util.ArrayList;
import java.util.List;

public class WxMessageRouter {

    private final List<WxMessageRouterRule> rules = new ArrayList<WxMessageRouterRule>();
    private final IService iService;
    private WxErrorExceptionHandler exceptionHandler;

    public WxMessageRouter(IService iService) {
        this.iService = iService;
    }

    public void setExceptionHandler(WxErrorExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    List<WxMessageRouterRule> getRules() {
        return this.rules;
    }

    public WxMessageRouterRule rule() {
        return new WxMessageRouterRule(this);
    }

    public AbstractWxXmlOutMessage route(final WxXmlMessage wxMessage) {

        final List<WxMessageRouterRule> matchRules = new ArrayList<WxMessageRouterRule>();
        for (final WxMessageRouterRule rule : rules) {
            if (rule.test(wxMessage)) {
                matchRules.add(rule);
                if (!rule.isReEnter()) {
                    break;
                }
            }
        }

        if (matchRules.size() == 0) {
            return null;
        }

        AbstractWxXmlOutMessage res = null;
        for (final WxMessageRouterRule rule : matchRules) {
            res = rule.service(wxMessage, iService, exceptionHandler);
        }
        return res;
    }
}
