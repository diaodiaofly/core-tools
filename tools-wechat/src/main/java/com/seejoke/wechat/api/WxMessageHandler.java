package com.seejoke.wechat.api;

import com.seejoke.wechat.entity.AbstractWxXmlOutMessage;
import com.seejoke.wechat.entity.WxXmlMessage;
import com.seejoke.wechat.exception.WxErrorException;

import java.util.Map;

public interface WxMessageHandler {

	AbstractWxXmlOutMessage handle(WxXmlMessage wxMessage, Map<String, Object> context, IService iService) throws WxErrorException;

}
