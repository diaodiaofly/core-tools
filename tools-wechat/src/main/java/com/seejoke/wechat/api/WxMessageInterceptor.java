package com.seejoke.wechat.api;

import com.seejoke.wechat.entity.WxXmlMessage;
import com.seejoke.wechat.exception.WxErrorException;

import java.util.Map;

public interface WxMessageInterceptor {

	boolean intercept(WxXmlMessage wxMessage, Map<String, Object> context, IService iService)
			throws WxErrorException;

}
