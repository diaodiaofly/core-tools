package com.seejoke.wechat.entity.outbuilder;

import com.seejoke.wechat.entity.WxXmlOutVoiceMessage;

/**
 * 语音消息builder
 * 
 * @author diaodiaofly
 */
public final class VoiceBuilder extends BaseBuilder<VoiceBuilder, WxXmlOutVoiceMessage> {

	private String mediaId;

	public VoiceBuilder mediaId(String mediaId) {
		this.mediaId = mediaId;
		return this;
	}

	@Override
    public WxXmlOutVoiceMessage build() {
		WxXmlOutVoiceMessage m = new WxXmlOutVoiceMessage();
		setCommon(m);
		m.setMediaId(mediaId);
		return m;
	}

}
