package com.seejoke.wechat.entity.outbuilder;

import com.seejoke.wechat.entity.WxXmlOutImageMessage;

/**
 * 图片消息builder
 * 
 * @author diaodiaofly
 */
public final class ImageBuilder extends BaseBuilder<ImageBuilder, WxXmlOutImageMessage> {

	private String mediaId;

	public ImageBuilder mediaId(String media_id) {
		this.mediaId = media_id;
		return this;
	}

	@Override
    public WxXmlOutImageMessage build() {
		WxXmlOutImageMessage m = new WxXmlOutImageMessage();
		setCommon(m);
		m.setMediaId(this.mediaId);
		return m;
	}

}
