package com.seejoke.wechat.entity.outbuilder;

import com.seejoke.wechat.entity.WxXmlOutVideoMessage;

/**
 * 视频消息builder
 * 
 * @author diaodiaofly
 *
 */
public final class VideoBuilder extends BaseBuilder<VideoBuilder, WxXmlOutVideoMessage> {

	private String mediaId;
	private String title;
	private String description;

	public VideoBuilder title(String title) {
		this.title = title;
		return this;
	}

	public VideoBuilder description(String description) {
		this.description = description;
		return this;
	}

	public VideoBuilder mediaId(String mediaId) {
		this.mediaId = mediaId;
		return this;
	}

	@Override
    public WxXmlOutVideoMessage build() {
		WxXmlOutVideoMessage m = new WxXmlOutVideoMessage();
		setCommon(m);
		m.setTitle(title);
		m.setDescription(description);
		m.setMediaId(mediaId);
		return m;
	}

}
