package com.seejoke.wechat.entity.msgbuilder;

import com.seejoke.wechat.api.WxConsts;
import com.seejoke.wechat.entity.WxMessage;

/**
 * 文本消息builder
 * 
 * <pre>
 * 用法: WxCustomMessage m = WxCustomMessage.TEXT().content(...).toUser(...).build();
 * </pre>
 * 
 * @author diaodiaofly
 *
 */
public final class TextBuilder extends BaseBuilder<TextBuilder> {
	private String content;

	public TextBuilder() {
		this.msgType = WxConsts.CUSTOM_MSG_TEXT;
	}

	public TextBuilder content(String content) {
		this.content = content;
		return this;
	}

	@Override
    public WxMessage build() {
		WxMessage m = super.build();
		m.setContent(this.content);
		return m;
	}
}
