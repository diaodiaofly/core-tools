package com.seejoke.wechat.entity;

import com.seejoke.wechat.api.WxConfig;
import com.seejoke.wechat.entity.outbuilder.*;
import com.seejoke.wechat.exception.AesException;
import com.seejoke.wechat.util.WXBizMsgCrypt;
import com.seejoke.wechat.util.xml.XStreamCDataConverter;
import com.seejoke.wechat.util.xml.XStreamTransformer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @author Administrator
 */
@XStreamAlias("xml")
public abstract class AbstractWxXmlOutMessage {

    @XStreamAlias("ToUserName")
    @XStreamConverter(value = XStreamCDataConverter.class)
    protected String toUserName;

    @XStreamAlias("FromUserName")
    @XStreamConverter(value = XStreamCDataConverter.class)
    protected String fromUserName;

    @XStreamAlias("CreateTime")
    protected Long createTime;

    @XStreamAlias("MsgType")
    @XStreamConverter(value = XStreamCDataConverter.class)
    protected String msgType;

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String toXml() {
        return XStreamTransformer.toXml((Class) this.getClass(), this);
    }

    public static String encryptMsg(WxConfig wxconfig, String replyMsg, String timeStamp, String nonce) throws AesException {
        WXBizMsgCrypt pc = new WXBizMsgCrypt(WxConfig.getInstance().getToken(), WxConfig.getInstance().getAesKey(), WxConfig.getInstance().getAppId());
        return pc.encryptMsg(replyMsg, timeStamp, nonce);
    }

    public static TextBuilder TEXT() {
        return new TextBuilder();
    }

    public static ImageBuilder IMAGE() {
        return new ImageBuilder();
    }

    public static VoiceBuilder VOICE() {
        return new VoiceBuilder();
    }

    public static VideoBuilder VIDEO() {
        return new VideoBuilder();
    }

    public static NewsBuilder NEWS() {
        return new NewsBuilder();
    }

    public static MusicBuilder MUSIC() {
        return new MusicBuilder();
    }

    @Override
    public String toString() {
        return "AbstractWxXmlOutMessage [toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime="
                + createTime + ", msgType=" + msgType + "]";
    }

}
