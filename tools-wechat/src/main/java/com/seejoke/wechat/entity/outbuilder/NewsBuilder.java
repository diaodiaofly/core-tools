package com.seejoke.wechat.entity.outbuilder;

import com.seejoke.wechat.entity.WxXmlOutNewsMessage;
import com.seejoke.wechat.entity.WxXmlOutNewsMessage.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * 图文消息builder
 * 
 * @author diaodiaofly
 */
public final class NewsBuilder extends BaseBuilder<NewsBuilder, WxXmlOutNewsMessage> {

	protected final List<Item> articles = new ArrayList<Item>();

	public NewsBuilder addArticle(Item item) {
		this.articles.add(item);
		return this;
	}

	@Override
    public WxXmlOutNewsMessage build() {
		WxXmlOutNewsMessage m = new WxXmlOutNewsMessage();
		for (Item item : articles) {
			m.addArticle(item);
		}
		setCommon(m);
		return m;
	}

}
