package com.seejoke.wechat.util.http;

import com.seejoke.wechat.entity.result.WxError;
import com.seejoke.wechat.exception.WxErrorException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class SimplePostRequestExecutor implements RequestExecutor<String, String> {
	@Override
	public String execute(CloseableHttpClient httpclient, String uri,String params)
			throws WxErrorException, ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(uri);
	
		if (params != null) {
			httpPost.setEntity(new StringEntity(params,"UTF-8"));
		}

		try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
			String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(responseContent);
			if(node.get("errcode")!=null && !(node.get("errcode").asInt()==0)){
				WxError error = WxError.fromJson(responseContent);
				throw new WxErrorException(error);
			}
			return responseContent;
		}
	}

}
