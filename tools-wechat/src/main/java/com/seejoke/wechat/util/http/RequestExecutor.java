package com.seejoke.wechat.util.http;

import com.seejoke.wechat.exception.WxErrorException;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public interface RequestExecutor<T, E> {

	T execute(CloseableHttpClient httpclient, String uri, E data)
			throws WxErrorException,  IOException;

}
