package com.seejoke.wechat;

import com.seejoke.wechat.api.IService;
import com.seejoke.wechat.api.WxConsts;
import com.seejoke.wechat.api.impl.WxServiceImpl;
import com.seejoke.wechat.entity.PreviewSender;
import com.seejoke.wechat.entity.SenderContent.Media;
import com.seejoke.wechat.entity.WxOpenidSender;
import com.seejoke.wechat.entity.result.SenderResult;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class MessageTest {

  IService iService = new WxServiceImpl();

  @Test
  public void should_preview_send_news_to_user() throws Exception {
    PreviewSender sender = new PreviewSender();
    sender.setTouser("oROCnuNihJnO9bnKOAORDFFriPgQ");
    sender.setMsgtype(WxConsts.MASS_MSG_MPNEWS);
    sender.setMpnews(new Media("QR3FgphTwoIpP1FZ-4c__cQTEeIHxMl7e_rWAfFYyfo"));
    SenderResult result = iService.sendAllPreview(sender);
    System.out.println(result.toString());
  }

  @Test
  public void should_send_news_to_user() throws Exception {
    WxOpenidSender sender = new WxOpenidSender();
    List<String> openidList = new ArrayList<>();
    openidList.add("oROCnuNihJnO9bnKOAORDFFriPgQ");
    openidList.add("oROCnuAQMnkPpEhsAYFzU-1xhKcQ");
    sender.setTouser(openidList);
    sender.setMsgtype(WxConsts.MASS_MSG_MPNEWS);
    sender.setMpnews(new Media("QR3FgphTwoIpP1FZ-4c__cQTEeIHxMl7e_rWAfFYyfo"));
    SenderResult result = iService.sendAllByOpenid(sender);
    System.out.println(result.toString());
  }
}
