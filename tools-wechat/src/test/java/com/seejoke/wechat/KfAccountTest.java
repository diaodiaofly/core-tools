package com.seejoke.wechat;

import com.seejoke.wechat.api.IService;
import com.seejoke.wechat.api.WxConsts;
import com.seejoke.wechat.api.impl.WxServiceImpl;
import com.seejoke.wechat.entity.KfAccount;
import com.seejoke.wechat.entity.KfSender;
import com.seejoke.wechat.entity.SenderContent.NewsList;
import com.seejoke.wechat.entity.SenderContent.NewsList.News;
import com.seejoke.wechat.entity.SenderContent.Text;
import com.seejoke.wechat.entity.result.KfAccountListResult;
import com.seejoke.wechat.entity.result.WxError;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

@Ignore
public class KfAccountTest {
  IService iService = new WxServiceImpl();

  @Test
  public void should_add_kf_account_successfully() throws Exception {
    WxError result = iService.addKfAccount(new KfAccount("ant@test", "ant", "ant"));
    System.out.println(result);
  }

  @Test
  public void should_update_kf_account_successfully() throws Exception {
    WxError result = iService.updateKfAccount(new KfAccount("ant@test", "ant", "ant"));
    System.out.println(result);
  }

  @Test
  public void should_delete_kf_account_successfully() throws Exception {
    WxError result = iService.deleteKfAccount(new KfAccount("ant@test", "ant", "ant"));
    System.out.println(result);
  }

  @Test
  public void should_get_all_kf_account_successfully() throws Exception {
    KfAccountListResult result = iService.getAllKfAccount();
    System.out.println(result);
  }

  @Test
  public void should_send_text_message_to_user_successfully() throws Exception {
    KfSender sender = new KfSender();
    sender.setTouser("oROCnuNihJnO9bnKOAORDFFriPgQ");
    sender.setMsgtype(WxConsts.MASS_MSG_TEXT);
    sender.setText(new Text("hi"));
    System.out.println(iService.sendMessageByKf(sender));
  }

  @Test
  public void should_send_news_message_to_user_successfully() throws Exception {
    KfSender sender = new KfSender();
    sender.setTouser("oROCnuNihJnO9bnKOAORDFFriPgQ");
    sender.setMsgtype(WxConsts.MASS_MSG_NEWS);
    sender.setNews(new NewsList(Arrays.asList(new News("title","desc","http://www.baidu.com","http://www.baidu.com"))));
    System.out.println(iService.sendMessageByKf(sender));
  }

  @Test
  public void should_update_kf_head_image_successfully() throws Exception {
    System.out.println(iService.updateKfHeadImage("oROCnuNihJnO9bnKOAORDFFriPgQ", new File("D:/wx/wx.jpg")));
  }
}
