package com.seejoke.wechat;

import com.seejoke.wechat.api.IService;
import com.seejoke.wechat.api.WxConsts;
import com.seejoke.wechat.api.impl.WxServiceImpl;
import com.seejoke.wechat.entity.WxUserList;
import com.seejoke.wechat.entity.WxUserList.WxUser;
import com.seejoke.wechat.entity.WxUserList.WxUser.WxUserGet;
import com.seejoke.wechat.entity.result.WxError;
import com.seejoke.wechat.entity.result.WxUserListResult;
import com.seejoke.wechat.entity.result.WxUserTagResult;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;

@Ignore
public class UserInfoTest {
  IService iService = new WxServiceImpl();


  @Test
  public void should_create_user_tag_successfully() throws Exception {
    WxUserTagResult result = iService.createUserTag("TestTag2");
    System.out.println(result);
  }

  @Test
  public void should_query_all_user_tag_successfully() throws Exception {
    WxUserTagResult result = iService.queryAllUserTag();
    System.out.println(result);
  }

  @Test
  public void should_update_user_tag_successfully() throws Exception {
    WxError result = iService.updateUserTagName(104, "TestTag2_update");
    System.out.println(result);
  }

  @Test
  public void should_delete_user_tag_successfully() throws Exception {
    WxError result = iService.deleteUserTag(104);
    System.out.println(result);
  }

  @Test
  public void should_batch_moving_user_to_tag_successfully() throws Exception {
    WxError result = iService.batchMovingUserToNewTag(Arrays.asList("oROCnuNihJnO9bnKOAORDFFriPgQ"), 104);
    System.out.println(result);
  }

  @Test
  public void should_batch_remove_user_to_tag_successfully() throws Exception {
    WxError result = iService.batchRemoveUserTag(Arrays.asList("oROCnuNihJnO9bnKOAORDFFriPgQ"), 104);
    System.out.println(result);
  }

  @Test
  public void should_query_all_user_under_by_tag_successfully() throws Exception {
    WxUserListResult result = iService.queryAllUserUnderByTag(2, null);
    System.out.println(result);
  }

  @Test
  public void should_update_user_remark_successfully() throws Exception {
    WxError result = iService.updateUserRemark("oROCnuNihJnO9bnKOAORDFFriPgQ", "Abel");
    System.out.println(result);
  }

  @Test
  public void should_query_user_info_successfully() throws Exception {
    WxUser result = iService.getUserInfoByOpenId(new WxUserGet("oROCnuNihJnO9bnKOAORDFFriPgQ",
        WxConsts.LANG_CHINA));
    System.out.println(result);
  }

  @Test
  public void should_batch_query_user_info_successfully() throws Exception {
    WxUserList result = iService.batchGetUserInfo(Arrays.asList(new WxUserGet("oROCnuNihJnO9bnKOAORDFFriPgQ",
        WxConsts.LANG_CHINA),new WxUserGet("oROCnuAQMnkPpEhsAYFzU-1xhKcQ",
        WxConsts.LANG_CHINA)));
    System.out.println(result);
  }

  @Test
  public void should_batch_get_user_id_successfully() throws Exception {
    WxUserListResult result = iService.batchGetUserOpenId(null);
    System.out.println(result);
  }

  @Test
  public void should_batch_add_user_to_black_list_successfully() throws Exception {
    WxError result = iService.batchAddUserToBlackList(Arrays.asList("oROCnuNihJnO9bnKOAORDFFriPgQ"));
    System.out.println(result);
  }

  @Test
  public void should_batch_remove_user_from_black_list_successfully() throws Exception {
    WxError result = iService.batchRemoveUserFromBlackList(Arrays.asList("oROCnuNihJnO9bnKOAORDFFriPgQ"));
    System.out.println(result);
  }

  @Test
  public void should_batch_get_users_from_black_list_successfully() throws Exception {
    WxUserListResult result = iService.batchGetUsersFromBlackList(null);
    System.out.println(result);
  }

}
